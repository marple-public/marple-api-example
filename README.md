# API example

## Python vs MATLAB
In this repository you can find two folders: Python and Matlab.
They contain examples on how to use the Marple API for both the Python and Matlab coding language.


## Python

Example Python scripts for using the Marple API.


### Setup

The script is dependent on Python packages `numpy` and `gym`:

```bash
pip install numpy
pip install gym
```

### Description

`run_sim.py` is the main python script. It uses [gym openai](https://gym.openai.com/) to run the classic control problem
of balancing a pole with a cart on rails. The parameters of the controller can be adjusted in `parameters.py`. Run the
experiment using

```bash
python run_sim.py
```

After the experiment has finished `upload_marple.py` will run and send the data to a Marple Server for visualisation.
This file has a number of parameters defined at the top. Adjust these as necessary:

```python
# Marple server URL
MARPLE_URL = "https://app.marpledata.com"
# User authentication credentials
# This token can be generated using the api_auth.py script or 
# can be found in the Marple app in your profile settings:
# https://app.marpledata.com/profile
ACCESS_TOKEN = "..."
# Folder on server to upload to
UPLOAD_DIR = 'run_sim' 
# Workbook to use to generate the share link. Make sure the project already exists on the server
WORKBOOK_NAME = 'cartpole'
```

## MATLAB
The examples can be found in `api_example.m`.
Make sure to fill in your access token before running the script.
The example script will upload an example .CSV file to your account in the folder specified (default `api-example`).

For the MATLAB example, only the uploading and importing of a file is implemented.

## Contact

Do not hesitate to contact us at

https://www.marpledata.com/contact

## License
[MIT](https://choosealicense.com/licenses/mit/)
