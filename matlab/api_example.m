marple_url = 'https://app.marpledata.com/api/v1';
access_token = % ENTER ACCESS TOKEN HERE
upload_dir = 'api-example'; % Enter the name of the folder where you want to upload on Marple.
example_file = 'examples_race.csv';

h1 = matlab.net.http.HeaderField('ContentType','json');
h2 = matlab.net.http.HeaderField('Authorization', ['Bearer ', access_token]);
headers = [h1, h2];

%% Get version to check connection
version_url = [marple_url, '/version'];

method = matlab.net.http.RequestMethod.GET;

request = matlab.net.http.RequestMessage(method, headers, '');
response = send(request, version_url);

version_number = response.Body.Data.message;
fprintf('Marple version: %s \n', version_number)

%% Upload file
upload_url = [marple_url, '/library/file/upload?path=', upload_dir];
method = matlab.net.http.RequestMethod.POST;

new_file = strcat('uploaded_file_', string(randi(1000)), '.csv');
copyfile(example_file, new_file);

file_provider = matlab.net.http.io.FileProvider(new_file);
provider = matlab.net.http.io.MultipartFormProvider("file", file_provider);

request = matlab.net.http.RequestMessage(method, headers, provider);
response = send(request, upload_url);

source_id = response.Body.Data.message.source_id;
path = response.Body.Data.message.path;
fprintf('Uploaded source id: %s \n', source_id)

%% Import file
import_url = [marple_url, '/library/file/import'];
method = matlab.net.http.RequestMethod.POST;

body = struct('path', path, 'plugin', 'csv_plugin');

request = matlab.net.http.RequestMessage(method, headers, body);
response = send(request, import_url);

fprintf('Start importing source id: %s \n', source_id)



