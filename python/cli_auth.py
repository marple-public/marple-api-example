#!/usr/bin/env python

import itertools
import os
import platform
import sys
import time

import requests

AUTH0_DOMAIN = 'marple.eu.auth0.com'
API_AUDIENCE = 'https://app.getmarple.io/api'

# Marple Device Application ID
client_id = '7Suh0Yl4EnTXVw55UOnHjrIIqnAprShi'


def req_device_code():
    payload = {'client_id': client_id,
               'scope': 'profile openid email',
               'audience': API_AUDIENCE}
    return requests.post(f"https://{AUTH0_DOMAIN}/oauth/device/code", data=payload)


def open_browser(uri):
    system = platform.system()
    if system == 'Linux':
        open_cmd = 'xdg-open'
    elif system == 'Darwin':
        open_cmd = 'open'
    elif system == 'Windows':
        open_cmd = 'explorer'
    os.system('{} "{}"'.format(open_cmd, uri))


def req_access_token(device_code):
    payload = {'client_id': client_id,
               'device_code': device_code,
               'grant_type': 'urn:ietf:params:oauth:grant-type:device_code'}
    return requests.post(f'https://{AUTH0_DOMAIN}/oauth/token', data=payload)


def turn_spinner(interval):
    spinner = itertools.cycle(['-', '\\', '|', '/'])
    for i in range(5*interval):
        sys.stdout.write(next(spinner))   # write the next character
        sys.stdout.flush()                # flush stdout buffer (actual character display)
        sys.stdout.write('\b')            # erase the last written char
        time.sleep(0.2)


def wait_for_login(device_req):
    t_start = time.time()
    while time.time() - t_start < device_req['expires_in']:
        turn_spinner(device_req['interval'])
        access_req = req_access_token(device_req['device_code'])
        payload = access_req.json()
        if access_req.status_code == 200:
            return payload
        if payload['error'] == 'authorization_pending':
            continue
        else:
            print("\n{}: {}".format(payload['error'], payload['error_description']))
            exit(1)

    print("Timed out while waiting for login.")
    exit(1)


def run():
    device_req = req_device_code().json()

    try:
        print("\nWelcome to the Marple API authentication tool")
        print("\nYour confirmation code is \033[1m{}\033[0m".format(device_req['user_code']))
        input("\n\33[33mPress enter\033[0m to open your browser and login or \033[91m^C\033[0m to exit")
        open_browser(device_req['verification_uri_complete'])
        print("\nWaiting for login to complete in browser... ", end='')
        tokens = wait_for_login(device_req)
    except KeyboardInterrupt:
        exit(0)

    print("\n\nYour access token:\n{}".format(tokens['access_token']))


if __name__ == '__main__':
    run()
