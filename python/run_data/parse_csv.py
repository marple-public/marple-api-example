import pandas as pd
import os


PLUGIN_NAME = 'CSV PARSER'
CHUNK_SIZE = int(1e3)


# REQUIRED FUNCTIONS

def get_headers(file_path):
    df = pd.read_csv(file_path, nrows=1)
    col_info = []
    for header in df.keys():
        col_info.append({'name': header})
    return col_info


def write_values(write_to_db, file_path):
    total_chunks = get_total_chunks(file_path)
    df = pd.read_csv(file_path, chunksize=CHUNK_SIZE)

    for index, chunk in enumerate(df):
        progress = index / total_chunks * 100
        write_to_db(chunk.values, progress=progress)


def get_metadata(file_path):
    return {}


# AUXILIARY FUNCTIONS

def get_total_chunks(file_path):
    header_amount = len(get_headers(file_path))
    file_size = os.stat(file_path).st_size
    num_rows_estimate = file_size / (header_amount * 8)
    return num_rows_estimate / CHUNK_SIZE
