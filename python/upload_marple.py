# Author: Jan Scheers
# Date: 25/11/2021

import requests
import requests.auth
import os
from urllib.parse import urljoin
import time

# Parameters
MARPLE_URL = "https://app.marpledata.com"
ACCESS_TOKEN = "..."
UPLOAD_DIR = 'run_sim'  # Folder on server to upload to
PROJECT_NAME = 'cartpole'  # Project to add the data to and generate a share link with. Make sure the project already exists


def upload_latest():
    # Setup
    api_url = urljoin(MARPLE_URL, '/api/v1')
    bearer_token = f"Bearer {ACCESS_TOKEN}"
    session = requests.Session()
    session.headers.update({"Authorization": bearer_token})

    # Get welcome message
    r = session.get('{}/version'.format(api_url))
    if r.status_code != 200:
        raise Exception('Could not connect to server at {}'.format(api_url))

    # Get root folders
    r = session.get('{}/library/tree'.format(api_url))
    tree = r.json()['message']
    folders = [child['path'] for child in tree['child_nodes']]

    # Check if UPLOAD_DIR already exist
    add_folder_body = {'path': '/', 'new_folder_name': UPLOAD_DIR}
    r = session.post('{}/library/folder/add/check'.format(api_url), json=add_folder_body)

    # Add UPLOAD_DIR folder if necessary
    if r.json()['message']['status'] == 'OK':
        r = session.post('{}/library/folder/add'.format(api_url), json=add_folder_body)

    local_data_dir = 'run_data'
    csv_files = filter(lambda s: 'csv' in s, os.listdir(local_data_dir))
    newest_file = sorted(csv_files)[-1]

    # Upload latest csv file
    file = open(os.path.join(local_data_dir, newest_file), 'rb')
    r = session.post('{}/library/file/upload'.format(api_url),
                      params={'path': UPLOAD_DIR},
                      files={'file': file})
    source_id, path = r.json()['message']['source_id'], r.json()['message']['path']
    print(f'{newest_file} has been uploaded onto fileserver and has been assigned id {source_id}')

    # Start parsing file into database
    csv_plugin = 'csv_plugin'
    body = {'path': path, 'plugin': csv_plugin}
    r = session.post('{}/library/file/import'.format(api_url), json=body)
    source_id = r.json()['message']['source_id']

    # Get import progress until ready
    r = session.get('{}/sources/status'.format(api_url), params={'id': source_id})
    status = r.json()['message'][0]['status']
    print(f'Import progress: {status}', end='')
    while status != 100:
        r = session.get('{}/sources/status'.format(api_url), params={'id': source_id})
        status = r.json()['message'][0]['status']
        print(f'\rImport progress: {status}', end='')
        time.sleep(1)
    print(f'\n{newest_file} has been imported into the database with id {source_id}')

    # check if PROJECT already exists
    params = {'name': PROJECT_NAME}
    r = session.get('{}/library/workbook/free'.format(api_url), params=params)
    if r.json()['message'] == True:
        raise Exception('Cannot generate share link: cannot find workbook "{}"'.format(PROJECT_NAME))

    # make new share link
    body = {'workbook_name': PROJECT_NAME, 'source_ids': [source_id]}
    r = session.post('{}/library/share/new'.format(api_url), json=body)
    share_id = r.json()['message']

    # Generate clickable link in terminal
    r = session.get('{}/library/share/{}/link'.format(api_url, share_id))
    print('View your data: {}'.format(r.json()['message']))
