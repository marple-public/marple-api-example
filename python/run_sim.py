#!/usr/bin/env python
# Author: Matthias Baert
# Date: 25/11/2021
#
# Run this file to execute simulation
import time
import csv
from cartpole_ai_gym import CartPoleEnv
from parameters import pos_target, pos_P_gain, vel_P_gain, angle_P_gain, omega_P_gain, sim_speed
from upload_marple import upload_latest

logged_data = {'time': []}


def main():
    env = CartPoleEnv()
    observation = env.reset()

    for ts in range(200):
        time.sleep(1/sim_speed)
        env.render()
        action, targets = PIDController(observation)
        observation, reward, done, info = env.step(action)
        log_states(ts, observation, targets)
        if done:
            break

    env.close()


def PIDController(state):
    pos = state[0]
    vel = state[1]
    angle = state[2]
    omega = state[3]

    pos_error = pos_target - pos
    vel_target = pos_error * pos_P_gain / 10

    vel_error = vel_target - vel
    angle_target = min(max(vel_error * vel_P_gain / 10, -1), 1)

    angle_error = angle_target - angle
    omega_target = angle_error * angle_P_gain

    omega_error = omega_target - omega
    alpha_target = omega_error * -omega_P_gain

    return alpha_target, [vel_target, angle_target, omega_target, alpha_target]


def log_states(ts, state, targets):
    pos = state[0]
    vel = state[1]
    angle = state[2]
    omega = state[3]
    [vel_target, angle_target, omega_target, alpha_target] = targets

    write_to_log('time', ts)
    write_to_log('pos', pos)
    write_to_log('pos_target', pos_target)
    write_to_log('vel', vel)
    write_to_log('vel_target', vel_target)
    write_to_log('angle', angle)
    write_to_log('angle_target', angle_target)
    write_to_log('omega', omega)
    write_to_log('omega_target', omega_target)
    write_to_log('alpha_target', alpha_target)


def write_to_log(key, value):
    global logged_data
    logged_data.setdefault(key, []).append(value)


def write_logs_to_file():
    file_path = 'run_data/sim_data_{}.csv'.format(int(time.time()))
    with open(file_path, mode='w', newline='') as f:
        writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(logged_data.keys())

        for i in range(len(logged_data['time'])):
            row = [logged_data[key][i] for key in logged_data.keys()]
            writer.writerow(row)


if __name__ == '__main__':
    main()
    write_logs_to_file()
    upload_latest()
