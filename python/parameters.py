# Author: Matthias Baert
# Date: 25/11/2021

# Targets
pos_target = 1
angle_target = 0
sim_speed = 100

# Gains
pos_P_gain = 0  # POSITION CONTROL, CHANGE SECOND
vel_P_gain = 0  # VELOCITY CONTROL, CHANGE FIRST
angle_P_gain = 5  # ANGLE CONTROL, NO NEED TO TOUCH
omega_P_gain = 4  # DO NOT TOUCH
